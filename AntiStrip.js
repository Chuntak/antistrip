/**
 * AntiStrip
 * Detects and warns users the ssl strip attack
 */

/* set initial security color on icon */
// chrome.browserAction.setBadgeText({ text: '0%' });
// chrome.browserAction.setBadgeBackgroundColor({ color: [0, 0, 255, 255] });

var globalDetails = "";
var beforeSendHeader = "";
var onSendHeader = "";
var onHeaderReceived = "";
var onBeforeRedirectDetails = "";
var onCompletedDetails = "";
var needWarning = 0;

var tabDictionary = {};
var requestDictionary = {};
var responseDictionary = {};
var currentTabId = -1;

var hstsList = [];//["google.com", "bing.com", "amex.co", "gogole.com"];
var cdnList = [];//["google.com", "amex.co", "americanexpress.com"];

saveListToStorage = function(type,list) {
    chrome.storage.sync.set({[type]: list}, function() {
          console.log("completed storing " + type);
    });
}


chrome.storage.sync.get(['hstsList'], function(result) {
     $.get( "https://s3.amazonaws.com/534list/sites.csv", function( data ) {
        console.log("Successfully loaded hsts from online src")
        if(Object.keys(result).length === 0) {
            hstsList = $.csv.toArrays(data);
            hstsList = [].concat.apply([], hstsList);
            saveListToStorage("hstsList", hstsList);
        } else {
            console.log("Completed loading hsts from storage");
            var temp = $.csv.toArrays(data);
            temp = [].concat.apply([], temp);
            var concat = temp.concat(result.hstsList);
            hstsList = concat.filter(function (item, pos) {return concat.indexOf(item) == pos}); //remove dup
        }
    }).fail(function() {
        console.log("failed to load from amazonaws, only use storage list");
        hstsList = result.hstsList;
    });
});

chrome.storage.sync.get(['cdnList'], function(result) {
     $.get( "https://s3.amazonaws.com/534list/cdn.csv", function( data ) {
        console.log("Successfully loaded cdn list from online src")
        if(Object.keys(result).length === 0) {
            cdnList = $.csv.toArrays(data);
            cdnList = [].concat.apply([], cdnList);
            saveListToStorage("cdnList", cdnList);
        } else {
            console.log("Completed loading cdnlist from storage");
            var temp = $.csv.toArrays(data);
            temp = [].concat.apply([], temp);
            var concat = temp.concat(result.cdnList);
            cdnList = concat.filter(function (item, pos) {return concat.indexOf(item) == pos}); //remove dup
        }
    }).fail(function() {
        console.log("failed to load from amazonaws, only use storage list");
        cdnList = result.cdnList;
    });;
});


beforeRequestFn = function(details) {}

beforeSendHeaderFn = function(details) {}

onSendHeaderFn = function(details) {
    // console.log("On Send Header");
    if(details.tabId != -1 && details.type === "main_frame") {
        onSendHeader = details;
        var urlSplit = details.url.split("/");
        if(inList(urlSplit[2], hstsList)) {
            requestDictionary[details.requestId] = details;
        }
    }
}
onHeadersReceived = function(details) {
    onHeaderReceived = details;
    if(requestDictionary[details.requestId]) {
        responseDictionary[details.requestId] = details;
        urlSplit = details.url.split("/");
        var isOnCurrentTab = details.tabId == currentTabId;
        if(urlSplit[0] === "http:") {
            console.log("Potential hack");
            tabDictionary[details.tabId] = 1;
            // if(isOnCurrentTab) showWarningPage();
        } else {
            if(tabDictionary[details.tabId] === 1){
                tabDictionary[details.tabId] = 0;
                console.log("false alarm");
                // if(isOnCurrentTab) closeWarningPage();
            }
        }
    }
}

var inList = function(url, list) {
    var regExp = "";
    for(var s in list) {
        regExp = new RegExp(list[s], "g");
        if(url.match(regExp)) {
            console.log(url + " matches " + regExp.toString())
            return true;
        }
    }
    return false;
}

onBeforeRedirect = function(details) {
    if(tabDictionary[details.tabId] == 1) showWarningPage();
    if(details.tabId != -1 && details.type === "main_frame") {
        onBeforeRedirectDetails = details;
        tabDictionary[details.tabId] = 0;
        console.log("false alarm");
        var urlSplit = details.url.split("/");
        var redirUrlSplit = details.redirectUrl.split("/");
        var isOnCurrentTab = details.tabId == currentTabId;
        if(inList(urlSplit[2], hstsList)) {
            console.log(urlSplit);
            console.log(details.redirectUrl);
            if(redirUrlSplit[0] === "http:") {
                console.log("HACKS");
                tabDictionary[details.tabId] = 1;
                // if(isOnCurrentTab) showWarningPage();
            } else { //https
                if(inList(redirUrlSplit[2], cdnList) && inList(urlSplit[2], cdnList)) {
                    tabDictionary[details.tabId] = 0;
                    console.log("Good case");
                    closeWarningPage();
                } else {
                    tabDictionary[details.tabId] = 1;
                    // if(isOnCurrentTab) showWarningPage();
                }
            }
        } else {
            if(redirUrlSplit[0] === "https:") {
                hstsList.push(urlSplit[2].replace("www.", "").replace("www2.", "").replace("ww3.", "").replace("ww4.", ""));
                if(!inList(urlSplit[2].replace("www.", "").replace("www2.", "").replace("ww3.", "").replace("ww4.", ""), cdnList))
                    cdnList.push(urlSplit[2].replace("www.", "").replace("www2.", "").replace("ww3.", "").replace("ww4.", ""));
                if(!inList(redirUrlSplit[2].replace("www.", "").replace("www2.", "").replace("ww3.", "").replace("ww4.", ""), cdnList))
                    cdnList.push(redirUrlSplit[2].replace("www.", "").replace("www2.", "").replace("ww3.", "").replace("ww4.", ""));
                saveListToStorage("hstsList", hstsList);
                saveListToStorage("cdnList", cdnList);
            }
        }
    }
}

onCompletedFn = function(details) {
    // console.log("completed request");
    if(details.tabId != -1 && details.type === "main_frame")
        onCompletedDetails = details;
    // if(needWarning) showWarningPage();
}

closeWarningPage = function() {
    chrome.tabs.executeScript({
        code :
        `
            var overlay = document.getElementById("warningOverlay");
            if(overlay) overlay.style.display = "none";
        `
    });
}

showWarningPage = function() {
    chrome.tabs.executeScript({
        code : 
        `   
            var overlay = document.getElementById("warningOverlay");
            if (typeof(overlay) == "undefined" || overlay == null) {
                overlay = document.createElement("DIV");
                overlay.setAttribute("id", "warningOverlay");
                overlay.style.position = "fixed";
                overlay.style.width = "100%";
                overlay.style.height = "100%";
                overlay.style.top = 0;
                overlay.style.left = 0;
                overlay.style.right = 0;
                overlay.style.bottom = 0;
                overlay.style.backgroundColor = "red";
                overlay.style.cursor = "pointer";
                overlay.style.webkitBackfaceVisibility = "hidden";
                overlay.style.zIndex = "9999999";
                document.body.insertBefore(overlay, document.body.children[0]);
                var label = document.createElement("P");
                var str = "WARNING SSL STRIPING DETECTED!";
                var result = str.fontsize(7);
                label.innerHTML = result;
                overlay.appendChild(label);
                
                var turnOff = function off() {
                    overlay.style.display = "none";
                    console.log("ON CLICK");
                }
                overlay.onclick = turnOff;
                console.log("DONE");
            } else {
                overlay.style.display = "block";
            }
        `
    })
}

var filter = {urls: ["<all_urls>"]};


// Fires when a request is about to occur. This event is sent before any TCP connection is made and can be used to cancel or redirect requests.
chrome.webRequest.onBeforeRequest.addListener(beforeRequestFn, filter, ["requestBody"]);
/**
 * Fires when a request is about to occur and the initial headers have been prepared. 
 * The event is intended to allow extensions to add, modify, and delete request headers (*). 
 * The onBeforeSendHeaders event is passed to all subscribers, so different subscribers may attempt to modify the request; 
 * see the Implementation details section for how this is handled. This event can be used to cancel the request.
 */
chrome.webRequest.onBeforeSendHeaders.addListener(beforeSendHeaderFn, filter, ["blocking","requestHeaders"]);

/**
 * Fires after all extensions have had a chance to modify the request headers, and presents the final (*) version. 
 * The event is triggered before the headers are sent to the network. This event is informational and handled asynchronously. 
 * It does not allow modifying or cancelling the request.
 */
chrome.webRequest.onSendHeaders.addListener(onSendHeaderFn, filter, []);

/**
 * Fires each time that an HTTP(S) response header is received. 
 * Due to redirects and authentication requests this can happen multiple times per request. 
 * This event is intended to allow extensions to add, modify, and delete response headers, such as incoming Set-Cookie headers. 
 * The caching directives are processed before this event is triggered, so modifying headers such as Cache-Control has no influence on the browser's cache.
 * It also allows you to cancel or redirect the request.
 */
chrome.webRequest.onHeadersReceived.addListener(onHeadersReceived, filter, ["responseHeaders"]);

chrome.webRequest.onBeforeRedirect.addListener(onBeforeRedirect, filter, ["responseHeaders"])


// Fires when a request has been processed successfully.
chrome.webRequest.onCompleted.addListener(onCompletedFn, filter, ["responseHeaders"]);


// Fired when a document, including the resources it refers to, is completely loaded and initialized.
chrome.webNavigation.onCompleted.addListener(function(details) {
    if(details.tabId == currentTabId && tabDictionary[currentTabId] === 1) {
        showWarningPage();
    } else {
        closeWarningPage();
    }
})

chrome.webNavigation.onHistoryStateUpdated.addListener(function(details){
    if(details.tabId == currentTabId && tabDictionary[currentTabId]) {
        showWarningPage();
    } else {
        closeWarningPage();
    }
})


// when a user switches tab, we set the currentTabId
chrome.tabs.onActivated.addListener(function(activeInfo) {
    currentTabId = activeInfo.tabId;
    console.log("Current tab: ", currentTabId);
    if(tabDictionary[currentTabId] === 1) showWarningPage();
});

